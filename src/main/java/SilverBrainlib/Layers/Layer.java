package SilverBrainlib.Layers;

import SilverBrainlib.neurons.Neuron;
import SilverBrainlib.neurons.NeuronModel;

public class Layer {
    protected int neuronCount = 0;

    public int getInputCount() {
        return inputCount;
    }

    protected int inputCount = 0;
    protected NeuronModel[] neurons;
    protected double[] output;

    public Layer(int nCount, int inputCount) {
        neuronCount = nCount;
        this.inputCount = inputCount;
        neurons = new Neuron[neuronCount];
    }

    public int getNeuronCount() {
        return neuronCount;
    }

    public NeuronModel[] getNeurons() {
        return neurons;
    }

    public double[] getOutput() {
        return output;
    }

    public double[] compute(double[] input) {
        double[] output = new double[neuronCount];
        for (int i = 0; i < neuronCount; i++) {
            output[i] = neurons[i].compPotential(input);
        }
        this.output = output;
        return output;
    }


}
