package SilverBrainlib.Layers;

import SilverBrainlib.ActivationFunctions.ActivationFunction;
import SilverBrainlib.neurons.Neuron;

public class PrimaryLayer extends Layer {
    public PrimaryLayer(int neuronCount, int inputCount, ActivationFunction function) {
        super(neuronCount, inputCount);
        for (int i = 0; i < neuronCount; i++) {
            neurons[i] = new Neuron(inputCount, function);
        }
    }
}
