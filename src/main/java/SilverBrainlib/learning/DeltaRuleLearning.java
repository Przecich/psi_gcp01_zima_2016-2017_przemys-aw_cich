package SilverBrainlib.learning;

import SilverBrainlib.ActivationFunctions.ActivationFunction;
import SilverBrainlib.Layers.Layer;
import SilverBrainlib.networks.NetworkModel;
import SilverBrainlib.neurons.Neuron;
import SilverBrainlib.neurons.NeuronModel;

import java.util.ArrayList;

public class DeltaRuleLearning extends Learning {



    public ArrayList errors = new ArrayList();

    public DeltaRuleLearning(NetworkModel network) {
        this.network = network;
    }

    @Override
    public double learnRecord(double[] input, double[] output) {
        double[] networkOutput = network.countPotential(input);


        Layer layer = network.getLayers()[0];
        ActivationFunction activationFunction = ((Neuron) (layer.getNeurons()[0])).getFunction();

        double error = 0.0;

        for (int j = 0; j < layer.getNeurons().length; j++) {

            NeuronModel neuron = layer.getNeurons()[j];

            double e = output[j] - networkOutput[j];
            // get activation function's derivative
            double functionDerivative = activationFunction.Derivative2(networkOutput[j]);

            // update dendrites
            for (int i = 0; i < neuron.dendrites.length; i++) {
                neuron.dendrites[i] += learningRate * e * functionDerivative * input[i];
            }

            // update threshold value
            ((Neuron) neuron).threshold += learningRate * e * functionDerivative;

            // sum error
            error += (e * e);
        }

        return error / 2;
    }


    @Override
    public double learnEpoch(double[][] inputw, double[][] output) {
        double error = 0;
        double mape = 0;
        for (int i = 0; i < inputw.length; i++) {

            error += learnRecord(inputw[i], output[i]);

        }

        errors.add(error);
        return error;
    }
}
