package SilverBrainlib.learning;
@FunctionalInterface
public interface ErrorListener {

    public void onErrorListener(double error);
}
