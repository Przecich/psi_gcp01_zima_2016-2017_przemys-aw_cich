package SilverBrainlib.learning;

import SilverBrainlib.Layers.Layer;
import SilverBrainlib.networks.NetworkModel;
import SilverBrainlib.neurons.Neuron;
import SilverBrainlib.neurons.NeuronModel;

import java.util.ArrayList;

public class PerceptronLearning extends Learning {


    public ArrayList errors = new ArrayList();






    public PerceptronLearning(NetworkModel network) {
        this.network = network;
    }

    @Override
    public double learnRecord(double[] input, double[] output) {

        double[] networkOutput = network.countPotential(input);
        Layer layer = network.getLayers()[0];
        double error = 0;

        for (int j = 0; j < layer.getNeurons().length; j++) {
            error = output[j] - networkOutput[j];
            if (error != 0) {
                Neuron pNeuron = (Neuron) layer.getNeurons()[j];
                for (int i = 0; i < input.length; i++) {
                    pNeuron.dendrites[i] += learningRate * error * input[i];
                }
                pNeuron.threshold += error * learningRate;
                error = Math.abs(error);

            }
        }
        return error;
    }


    @Override
    public double learnEpoch(double[][] inputw, double[][] output) {
        double error = 0;
        double mape = 0;
        for (int i = 0; i < inputw.length; i++) {

            error += learnRecord(inputw[i], output[i]);

        }
        errors.add(error);


        return error;
    }

}
