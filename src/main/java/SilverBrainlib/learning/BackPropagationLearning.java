package SilverBrainlib.learning;

import SilverBrainlib.ActivationFunctions.ActivationFunction;
import SilverBrainlib.Layers.Layer;
import SilverBrainlib.networks.NetworkModel;
import SilverBrainlib.neurons.Neuron;
import SilverBrainlib.neurons.NeuronModel;

public class BackPropagationLearning extends Learning {

    //TODO Refactor and optimize




    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }





    private double[][] neuronErrors = null;

    private double[][][] weightsUpdates = null;

    private double[][] thresholdsUpdates = null;


    public BackPropagationLearning(NetworkModel network) {
        this.network = network;


        neuronErrors = new double[network.getLayers().length][];
        weightsUpdates = new double[network.getLayers().length][][];
        thresholdsUpdates = new double[network.getLayers().length][];

        for (int i = 0; i < network.getLayers().length; i++) {
            Layer layer = network.getLayers()[i];

            neuronErrors[i] = new double[layer.getNeurons().length];
            weightsUpdates[i] = new double[layer.getNeurons().length][];
            thresholdsUpdates[i] = new double[layer.getNeurons().length];


            for (int j = 0; j < weightsUpdates[i].length; j++) {
                weightsUpdates[i][j] = new double[layer.getInputCount()];
            }
        }
    }


    @Override
    public double learnRecord(double[] input, double[] output) {

        double[] out = network.countPotential(input);


        double error = CalculateError(output);


        CalculateUpdates(input);


        UpdateNetwork();

        return error;
    }


    @Override
    public double learnEpoch(double[][] input, double[][] output) {
        double error = 0.0;
        for (int i = 0; i < input.length; i++) {
            error += learnRecord(input[i], output[i]);
        }


        return error;
    }

    @Override
    public void learn(double[][] input, double[][] output) {

        while (true) {
            double error;
            error = learnEpoch(input, output);
            notifyListeners(error);

            if (error < toleration) break;
        }

    }

    private void notifyListeners(double error) {
        for (ErrorListener listener : listeners)
            listener.onErrorListener(error);
    }

    private double CalculateError(double[] desiredOutput) {

        Layer layer, layerNext;

        double[] errors, errorsNext;

        double error = 0, e, sum;

        double output;

        int layersCount = network.getLayers().length;


        ActivationFunction function = ((Neuron) network.getLayers()[0].getNeurons()[0]).getFunction();


        layer = network.getLayers()[layersCount - 1];
        errors = neuronErrors[layersCount - 1];


        for (int i = 0; i < layer.getNeurons().length; i++) {
            output = layer.getNeurons()[i].getAxon();

            e = desiredOutput[i] - output;

            errors[i] = e * function.Derivative2(output);

            error += (e * e);
        }


        for (int j = layersCount - 2; j >= 0; j--) {
            layer = network.getLayers()[j];
            layerNext = network.getLayers()[j + 1];
            errors = neuronErrors[j];
            errorsNext = neuronErrors[j + 1];


            for (int i = 0; i < layer.getNeurons().length; i++) {
                sum = 0.0;

                for (int k = 0; k < layerNext.getNeurons().length; k++) {
                    sum += errorsNext[k] * layerNext.getNeurons()[k].dendrites[i];
                }
                errors[i] = sum * function.Derivative2(layer.getNeurons()[i].getAxon());
            }
        }


        return error / 2.0;
    }

    private void CalculateUpdates(double[] input) {

        NeuronModel neuron;

        Layer layer, layerPrev;

        double[][] layerWeightsUpdates;

        double[] layerThresholdUpdates;

        double[] errors;

        double[] neuronWeightUpdates;

        layer = network.getLayers()[0];
        errors = neuronErrors[0];
        layerWeightsUpdates = weightsUpdates[0];
        layerThresholdUpdates = thresholdsUpdates[0];



        double tempError;


        for (int i = 0; i < layer.getNeurons().length; i++) {
            neuron = layer.getNeurons()[i];
            tempError = errors[i] * learningRate;
            neuronWeightUpdates = layerWeightsUpdates[i];


            for (int j = 0; j < neuronWeightUpdates.length; j++) {

                neuronWeightUpdates[j] = tempError * input[j];
            }


            layerThresholdUpdates[i] =  tempError;
        }


        for (int k = 1; k < network.getLayers().length; k++) {
            layerPrev = network.getLayers()[k - 1];
            layer = network.getLayers()[k];
            errors = neuronErrors[k];
            layerWeightsUpdates = weightsUpdates[k];
            layerThresholdUpdates = thresholdsUpdates[k];


            for (int i = 0; i < layer.getNeurons().length; i++) {
                neuron = layer.getNeurons()[i];
                tempError = errors[i] *learningRate;
                neuronWeightUpdates = layerWeightsUpdates[i];


                for (int j = 0; j < neuronWeightUpdates.length; j++) {

                    neuronWeightUpdates[j] = tempError * layerPrev.getNeurons()[j].getAxon();

                }


                layerThresholdUpdates[i] = tempError;
            }


        }
    }


    private void UpdateNetwork() {

        Neuron neuron;

        Layer layer;

        double[][] layerWeightsUpdates;

        double[] layerThresholdUpdates;

        double[] neuronWeightUpdates;


        for (int i = 0; i < network.getLayers().length; i++) {
            layer = network.getLayers()[i];
            layerWeightsUpdates = weightsUpdates[i];
            layerThresholdUpdates = thresholdsUpdates[i];


            for (int j = 0; j < layer.getNeurons().length; j++) {
                neuron = (Neuron) layer.getNeurons()[j];
                neuronWeightUpdates = layerWeightsUpdates[j];


                for (int k = 0; k < neuron.dendrites.length; k++) {

                    neuron.dendrites[k] += neuronWeightUpdates[k];

                }

                neuron.threshold += layerThresholdUpdates[j];
            }
        }
    }
}



