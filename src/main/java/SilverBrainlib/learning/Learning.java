package SilverBrainlib.learning;

import SilverBrainlib.networks.NetworkModel;


import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public abstract class Learning {
    abstract public double learnRecord(double[] input, double[] output);

    abstract public double learnEpoch(double[][] inputw, double[][] output);

    public void learn(double[][] inputw, double[][] output) {
    }

    public void addErrorListener(ErrorListener errorListener){
        listeners.add(errorListener);

        System.out.println(Collections.max(Arrays.asList(0.9,0.1)));
    }

    protected double learningRate = 0.1;
    protected NetworkModel network;

    public void setToleration(double toleration) {
        this.toleration = toleration;
    }

    protected double toleration = 0.01;

    protected ArrayList<ErrorListener> listeners = new ArrayList<>();


}
