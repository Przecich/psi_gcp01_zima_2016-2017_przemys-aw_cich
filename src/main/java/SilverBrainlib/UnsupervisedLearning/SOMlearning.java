package SilverBrainlib.UnsupervisedLearning;

import SilverBrainlib.neurons.NeuronKohonen;

import java.util.ArrayList;

public class SOMlearning {

    NeuronKohonen[] neuronKohonens;

    public SOMlearning(NeuronKohonen[] neuronKohonens) {
        this.neuronKohonens = neuronKohonens;
        width=neuronKohonens.length;
    }

    private double learningRadius = 100;

    // squared learning radius multiplied by 2 (precalculated value to speed up computations)
    private double squaredRadius2 = 2 * learningRadius*learningRadius;

    int width;

    public double run(double[] input) {
        double error = 0.0;

        // compute the network
        //for(int i=0;i<neuronKohonens.length;i++)neuronKohonens[i].compPotential(input);
        int winner = getWinner(input);


        // get layer of the network

        if ( learningRadius == 0 ) {
            // update weight of the winner only
            for (int i = 0; i < neuronKohonens[winner].dendrites.length; i++) {
                // calculate the error

                double e = input[i] - neuronKohonens[winner].dendrites[i];
                error += Math.abs(e);
                // update dendrites
                neuronKohonens[winner].dendrites[i] += e * 1;
            }
        }else
        {
            // winner's X and Y
            int wx = winner % width;
            int wy = winner / width;

            // walk through all neurons of the layer
            for ( int j = 0; j < neuronKohonens.length; j++ )
            {


                int dx = ( j % width ) - wx;
                int dy = ( j / width ) - wy;

                // update factor ( Gaussian based )
                double factor = Math.exp( -(double) ( dx * dx + dy * dy ) / squaredRadius2 );

                // update weight of the neuron
                for ( int i = 0; i < neuronKohonens[j].dendrites.length; i++ )
                {
                    // calculate the error
                    double e = ( input[i] - neuronKohonens[j].dendrites[i] ) * factor;
                    error += Math.abs( e );
                    // update weight
                    neuronKohonens[j].dendrites[i] += e * 0.1;
                }
            }
        }

        return error;

    }


    public double learn(double[][] inputw) {

        double error = 0.0;

        for (int i = 0; i < inputw.length; i++) {
            error += run(inputw[i]);
        }


        return error;
    }

    public int getWinner(double[] input) {
        ArrayList<Double> list = new ArrayList();
        for (int i = 0; i < neuronKohonens.length; i++)
            list.add(neuronKohonens[i].compPotential(input));

        double min = list.get(0);
        int index = 0;
        int minIndex = 0;

        for (int i = 1; i < neuronKohonens.length; i++) {
            if (list.get(i) < min) {
                // found new MIN value
                min = list.get(i);
                minIndex = i;
            }
        }

        return minIndex;
    }

}
