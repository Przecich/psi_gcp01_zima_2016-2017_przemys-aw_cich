package SilverBrainlib.UnsupervisedLearning;

import java.util.ArrayList;

public class WTALearning {
    InstarNeuron[] instarNeuron;

    public WTALearning(InstarNeuron[] instarNeuron) {
        this.instarNeuron = instarNeuron;
    }

    public double run(double[] input) {


        int index = getWinner(input);

        instarNeuron[index].updateWeights(input);
        double error = 0.0;
        for (int l = 0; l < instarNeuron[index].dendrites.length; l++) {
            error += input[l] - instarNeuron[index].dendrites[l];
        }
        return error;
    }

    public double learn(double[][] input) {
        double error = 0.0;
        for (int j = 0; j < input.length; j++) {
            error += run(input[j]);
        }
        return error;
    }

    public int getWinner(double[] input) {

        ArrayList<Double> list = new ArrayList();
        for (int i = 0; i < instarNeuron.length; i++)
            list.add(instarNeuron[i].compPotential(input));
        int index = 0;
        double largest = list.get(0);

        for (int i = 1; i < list.size(); i++) {
            if (largest < list.get(i)) {
                index = i;
                largest = list.get(i);
            }
        }
        return index;
    }

}
