package SilverBrainlib.UnsupervisedLearning;

import SilverBrainlib.neurons.NeuronModel;

public class InstarNeuron extends NeuronModel {
    double[] firstWeights;
    double error;

    InstarNeuron(int inputCount) {
        super(inputCount);
        normalizeWeights();

        firstWeights = new double[dendrites.length];
        for (int i = 0; i < dendrites.length; i++) firstWeights[i] = dendrites[i];

    }

    @Override
    public double compPotential(double[] input) {
        double sum = 0;
        for (int i = 0; i < input.length; i++) {
            sum += dendrites[i] * input[i];

        }
        this.axon = sum;
        return sum;
    }

    public void normalizeWeights() {
        double weightLength = 0;
        for (int i = 0; i < dendrites.length; i++) weightLength += dendrites[i] * dendrites[i];

        weightLength = Math.sqrt(weightLength);
        for (int i = 0; i < dendrites.length; i++) dendrites[i] = dendrites[i] / weightLength;

    }

    public void updateWeights(double[] input) {
        for (int i = 0; i < dendrites.length; i++) dendrites[i] = dendrites[i] + 0.1 * (input[i] - dendrites[i]);
        normalizeWeights();
    }

    @Override
    public String toString() {
        String a = "";
        for (int i = 0; i < dendrites.length; i++) a += "Wp: " + firstWeights[i] + " W: " + dendrites[i] + " " + "\n";
        return a;
    }
}
