package SilverBrainlib.UnsupervisedLearning;

import SilverBrainlib.Layers.Layer;
import SilverBrainlib.learning.Learning;
import SilverBrainlib.networks.NetworkModel;
import SilverBrainlib.neurons.Neuron;
import SilverBrainlib.neurons.NeuronModel;

import java.util.ArrayList;

public class HebbLearning extends Learning {
    NeuronModel neuron;
    //type of heban learning
    int type = 0;
    NetworkModel network;
    public ArrayList errors = new ArrayList();
    public ArrayList<Double> mse = new ArrayList();
    public ArrayList<Double> mapes = new ArrayList();
    public long numAbove;
    public int numBelow = 0;


    double learningratio = 0.1;
    double forgetfulness = 0.1;


    public HebbLearning(NetworkModel network) {
        this.network = network;
    }

    @Override
    public double learnRecord(double[] input, double[] output) {

        double[] networkOutput = network.countPotential(input);
        Layer layer = network.getLayers()[0];
        double error = 0;
       /* if(error>0)numAbove++;
        else numBelow++;*/
        for (int j = 0; j < layer.getNeurons().length; j++) {
            error = output[j] - networkOutput[j];
            if (error != 0) {
                Neuron pNeuron = (Neuron) layer.getNeurons()[j];
                pNeuron.threshold = 1;
                for (int i = 0; i < input.length; i++) {
                    if (type == 0) pNeuron.dendrites[i] += learningratio * networkOutput[j] * input[i];

                    else if (type == 1)
                        pNeuron.dendrites[i] += learningratio * (input[i] - (forgetfulness * pNeuron.dendrites[i])) * networkOutput[j];
                    else if (type == 2)
                        pNeuron.dendrites[i] += learningratio * (input[i] * networkOutput[j] - networkOutput[j] * networkOutput[j] * pNeuron.dendrites[i]);
                    //System.out.println(pNeuron.dendrites[i]);

                }
                //pNeuron.threshold += networkOutput[j] * learningratio;
                error = Math.abs(error);

            }
        }
        return error;
    }

    public double run2(double[] input, double[] output) {

        double[] networkOutput = network.countPotential(input);

        double error = 0;
       /* if(error>0)numAbove++;
        else numBelow++;*/
        for (int l = 0; l < network.getLayers().length; l++) {
            Layer layer = network.getLayers()[l];
            for (int j = 0; j < layer.getNeurons().length; j++) {
                double[] inputLocal;
                if (l == 0) inputLocal = input;
                else inputLocal = layer.getOutput();

                Neuron pNeuron = (Neuron) layer.getNeurons()[j];
                pNeuron.threshold = 1;
                for (int i = 0; i < input.length; i++) {
                    if (type == 0) pNeuron.dendrites[i] += learningratio * networkOutput[j] * inputLocal[i];

                    else if (type == 1)
                        pNeuron.dendrites[i] += learningratio * (inputLocal[i] - (forgetfulness * pNeuron.dendrites[i])) * networkOutput[j];
                    else if (type == 2)
                        pNeuron.dendrites[i] += learningratio * (inputLocal[i] * networkOutput[j] - networkOutput[j] * networkOutput[j] * pNeuron.dendrites[i]);
                    //System.out.println(pNeuron.dendrites[i]);

                }
                //pNeuron.threshold += networkOutput[j] * learningratio;
                error = Math.abs(error);


            }
        }
        return error;
    }


    @Override
    public double learnEpoch(double[][] inputw, double[][] output) {
        double error = 0;
        double mape = 0;
        for (int i = 0; i < inputw.length; i++) {

            error += run2(inputw[i], output[i]);

        }
        errors.add(error);


        return error;
    }

}
