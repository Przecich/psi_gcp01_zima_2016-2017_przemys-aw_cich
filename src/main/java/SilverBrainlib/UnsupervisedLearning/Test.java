package SilverBrainlib.UnsupervisedLearning;

import SilverBrainlib.neurons.Neuron;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;

public class Test {
    public  static  HashMap<Integer,String> neuronShape=new HashMap<>();
    /*  public static double [][]
              learnData={
              {0.97,0.2},
              {1.0,0.0},
              {-0.72,0.7},
              {-0.67,0.74},
              {-0.8,0.6},
              {0.0,-1.0},
              {0.2,-0.97},
              {-0.3,-0.95}
      };*/
    public static double[][] learnData = {
            {1, 1, 1, -1, -1, 1},
            {1, 1, 1, 1, -1, -1},
            {1, 1, -1, 1, 1, -1},
            {1, 1, -1, -1, 1, 1},
            {1, 1, -1, -1, 1, 1},
            {-1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, 1},
            {1, -1, -1, -1, -1, -1}
    };
    public static double [] well={
            -1,-1,-1,-1,1,1
    };

    public static double [] reverse(double [] array){
        double [] result=new double[array.length];
        for(int i=0;i<array.length;i++){
            result[i]=array[i]*-1;
        }

        return result;
    }
    public static String[] ch = {"--|", "|--", "::", "_|-", "-|_", "e1", "e2", "e3"};
    public static InstarNeuron[] neurons = new InstarNeuron[6];

    private static void normalizeData() {
        for (int i = 0; i < learnData.length; i++) {
            double length = vectorLength(learnData[i]);
            for (int j = 0; j < learnData[i].length; j++) {
                learnData[i][j] = learnData[i][j] / length;
            }
        }
    }

    private static double vectorLength(double[] tab) {
        double sum = 0;
        for (int i = 0; i < tab.length; i++) {
            sum += Math.abs(tab[i]);
        }
        sum = Math.sqrt(sum);
        return sum;
    }

    public String findBlock(InstarNeuron [] neurons,double [] wellStruckt){
        WTALearning wtaLearning = new WTALearning(neurons);
        neuronShape.containsKey(wtaLearning.getWinner(Test.reverse((well))));
        return neuronShape.get(wtaLearning.getWinner(Test.reverse((well))));


    }
    public static void main(String[] args) {
        Path path = Paths.get("output.txt");
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {


        for(int j=0;j<50;j++) {
            System.out.println("aa");
            writer.write("Neuron nr: "+j);
            normalizeData();
            for (int i = 0; i < neurons.length; i++) {
                neurons[i] = new InstarNeuron(6);
            }

            WTALearning wtaLearning = new WTALearning(neurons);
            for (int i = 0; i < 15000; i++) {
                writer.write(""+wtaLearning.learn(learnData)+"\n");
            }
            //wypisz wagi
            for (InstarNeuron neuron : neurons) writer.write(neuron.toString()+"\n");

            //sprawdz axon
           // for (double[] data : learnData) System.out.println(neurons[0].compPotential(data)+"\n");

            for (int i = 0; i < learnData.length; i++) {
                writer.write(ch[i] + " " + wtaLearning.getWinner(learnData[i])+"\n");
                neuronShape.put(Integer.valueOf(wtaLearning.getWinner(learnData[i])), ch[i]);
            }
            Test test = new Test();
            writer.write("WYNIK :"+"\n");
            writer.write(test.findBlock(neurons, well)+"\n");
            }
            }
            catch (Exception e){

            }
        }
    }


