package SilverBrainlib.UnsupervisedLearning;

import SilverBrainlib.neurons.NeuronKohonen;

public class SOMtest {
    public static double[][] learnData = {
            {1, 1, 1, -1, -1, 1},
            {1, 1, 1, 1, -1, -1},
            {1, 1, -1, 1, 1, -1},
            {1, 1, -1, -1, 1, 1},
            {1, 1, -1, -1, 1, 1},
            {-1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, 1},
            {1, -1, -1, -1, -1, -1}
    };

    public static String[] ch = {"--|", "|--", "::", "_|-", "-|_", "e1", "e2", "e3"};
    public static NeuronKohonen[] neurons = new NeuronKohonen[2*2];

    private static void normalizeData() {
        for (int i = 0; i < learnData.length; i++) {
            double length = vectorLength(learnData[i]);
            for (int j = 0; j < learnData[i].length; j++) {
                learnData[i][j] = learnData[i][j] / length;
            }
        }
    }

    private static double vectorLength(double[] tab) {
        double sum = 0;
        for (int i = 0; i < tab.length; i++) {
            sum += Math.abs(tab[i]);
        }
        sum = Math.sqrt(sum);
        return sum;
    }

    public static void main(String[] args) {
        //normalizeData();
        for (int i = 0; i < neurons.length; i++) {
            neurons[i] = new NeuronKohonen(6);

        }

        SOMlearning wtaLearning = new SOMlearning(neurons);
        for (int i = 0; i < 500; i++) {
            System.out.println(wtaLearning.learn(learnData));
        }
        System.out.println("---------------------");
        //mapa
        for (int l = 0; l < learnData.length; l++) {
            wtaLearning.run(learnData[l]);
            wtaLearning.getWinner(learnData[l]);
        }
        //wypisz wagi


        //sprawdz axon
        //
        //for(NeuronKohonen kohonen:neurons) System.out.println(kohonen.toString());
        for (int i = 0; i < learnData.length; i++) {
            System.out.println(ch[i] + " " + wtaLearning.getWinner(learnData[i]));
        }


    }
}
