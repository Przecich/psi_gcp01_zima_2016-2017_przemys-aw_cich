package SilverBrainlib.Tests;

import SilverBrainlib.ActivationFunctions.ActivateBipolarSigmoid;
import SilverBrainlib.learning.BackPropagationLearning;
import SilverBrainlib.networks.Network;
import SilverBrainlib.networks.NetworkModel;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class PropagationTest {

    @Test(timeout = 1000)
    public void propagationPass() {
        double[][] data = {{0, 0}, {1, 1}, {1, 0}, {0, 1}};
        double[][] output = {{0}, {0}, {1}, {1}};
        NetworkModel network = new Network(new ActivateBipolarSigmoid(), 2, 2, 1);
        BackPropagationLearning teacher = new BackPropagationLearning(network);

        ArrayList<Double> list=new ArrayList<>();

        teacher.addErrorListener(e -> list.add(e));
        teacher.learn(data,output);

        assertEquals("Error value is not correct",0.0996,list.get(list.size()-1), 0.1);
    }

}