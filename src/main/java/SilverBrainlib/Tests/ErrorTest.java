package SilverBrainlib.Tests;

import SilverBrainlib.ActivationFunctions.ActivateSigmoid;
import SilverBrainlib.neurons.Neuron;
import SilverBrainlib.neurons.NeuronModel;
import org.junit.Test;


public class ErrorTest {

    @Test(expected = IllegalArgumentException.class)
    public void exceptionCatch() {
        double[][] data = {{0, 0, 1}, {1, 1, 1}, {1, 0, 1}, {0, 1, 1}};
        double[][] output = {{0}, {0}, {1}, {1}};

        NeuronModel neuronModel = new Neuron(2, new ActivateSigmoid());
        neuronModel.compPotential(data[0]);
    }

}
