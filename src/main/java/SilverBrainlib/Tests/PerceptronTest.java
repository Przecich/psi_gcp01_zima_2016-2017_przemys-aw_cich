package SilverBrainlib.Tests;

import SilverBrainlib.ActivationFunctions.ActivateThreshold;
import SilverBrainlib.learning.PerceptronLearning;
import SilverBrainlib.networks.Network;
import SilverBrainlib.networks.NetworkModel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PerceptronTest {
    @Test(timeout = 1000)
    public void testPerceptron(){
        double [] []input={{1,1},{1,0},{0,1},{0,0}};
        double [][]output={{1},{0},{0},{0}};

        NetworkModel networkModel=new Network(new ActivateThreshold(),2,1);
        PerceptronLearning learning=new PerceptronLearning(networkModel);
        double error=0;
        while(true){
                error=learning.learnEpoch(input,output);
                if(error<0.1)break;
        }
        assertEquals("AND 1 1 not equals 1",1,( networkModel.countPotential(new double[] {1,1}))[0],0.0001);

    }
}
