package SilverBrainlib.ActivationFunctions;

public class ActivateSigmoid implements ActivationFunction {
    private double alpha = 1;

    @Override
    public double activationfunction(double a) {
        return (1 / (1 + Math.exp(-alpha * a)));
    }

    ///
    public double Derivative(double x) {
        return x;
    }

    public double Derivative2(double y) {
        return y;
    }
}
