package SilverBrainlib.ActivationFunctions;

public class ActivateBipolarSigmoid implements ActivationFunction {
    double alpha = 1;

    @Override
    public double activationfunction(double a) {

        return ((2 / (1 + Math.exp(-alpha * a))) - 1);
    }


    public double Derivative(double x) {

        double y = activationfunction(x);
        return (alpha * (1 - y * y) / 2);
    }

    public double Derivative2(double y) {

        return (alpha * (1 - y * y) / 2);
    }
}
