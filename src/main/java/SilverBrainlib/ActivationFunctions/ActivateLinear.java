package SilverBrainlib.ActivationFunctions;

public class ActivateLinear implements ActivationFunction {
    @Override
    public double Derivative2(double y) {
        return 0;
    }

    @Override
    public double Derivative(double x) {
        return 0;
    }

    @Override
    public double activationfunction(double a) {
        return -a;
    }
}
