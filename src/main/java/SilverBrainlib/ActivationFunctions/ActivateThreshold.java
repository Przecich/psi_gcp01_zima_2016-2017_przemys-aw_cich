package SilverBrainlib.ActivationFunctions;

public class ActivateThreshold implements ActivationFunction {
    @Override
    public double Derivative(double x) {
        return x;
    }

    @Override
    public double Derivative2(double a) {
        return a;
    }

    @Override
    public double activationfunction(double a) {
        if (a > 0) return 1;
        else return 0;
    }
}

