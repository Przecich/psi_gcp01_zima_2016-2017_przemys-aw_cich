package SilverBrainlib.ActivationFunctions;

public interface ActivationFunction {
    double activationfunction(double a);

    ///
    public double Derivative(double x);


    public double Derivative2(double y);

}
