package SilverBrainlib.networks;

import SilverBrainlib.ActivationFunctions.ActivationFunction;
import SilverBrainlib.Layers.PrimaryLayer;

public class Network extends NetworkModel {
    public Network(ActivationFunction function, int inputCount, int... neuronCount) {
        super(neuronCount.length, inputCount);


        for (int i = 0; i < layers.length; i++)
            layers[i] = new PrimaryLayer(neuronCount[i], (i == 0) ? inputCount : neuronCount[i - 1], function);
    }
}
