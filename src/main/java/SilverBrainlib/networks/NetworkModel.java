package SilverBrainlib.networks;

import SilverBrainlib.Layers.Layer;

public class NetworkModel {
    protected int layerCount;
    protected int inputCount;

    protected double[] output;

    public Layer[] getLayers() {
        return layers;
    }

    protected Layer[] layers;

    NetworkModel(int layerCount, int inputCount) {
        this.inputCount = inputCount;
        this.layerCount = layerCount;

        layers = new Layer[layerCount];
    }

    public double[] countPotential(double[] input) {
        double[] threadSafeOutput = input;

        for (Layer layer : layers) {
            threadSafeOutput = layer.compute(threadSafeOutput);
        }

        output = threadSafeOutput;
        return output;
    }

}
