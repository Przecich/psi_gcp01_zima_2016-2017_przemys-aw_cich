package SilverBrainlib.neurons;

import java.util.Random;

public abstract class NeuronModel {
    int dendritesCount = 0;

    public double getAxon() {
        return axon;
    }

    protected double axon;

    public double[] dendrites;

    protected static Random rand = new Random();

    public NeuronModel(int intputCount) {
        this.dendritesCount = intputCount;
        dendrites = new double[intputCount];
        for (int i = 0; i < dendrites.length; i++) dendrites[i] = rand.nextDouble();
    }

    abstract public double compPotential(double[] input);

}
