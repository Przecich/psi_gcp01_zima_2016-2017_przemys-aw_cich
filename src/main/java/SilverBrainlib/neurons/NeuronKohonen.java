package SilverBrainlib.neurons;

public class NeuronKohonen extends NeuronModel {
    public double[] firstWeights;

    public NeuronKohonen(int inputCount) {
        super(inputCount);

        firstWeights = new double[dendrites.length];
        for (int i = 0; i < dendrites.length; i++) firstWeights[i] = dendrites[i];
    }

    @Override
    public double compPotential(double[] input) {
        double dif = 0.0;

        for (int i = 0; i < dendritesCount; i++) {
            dif += Math.abs(dendrites[i] - input[i]);
        }

        this.axon = dif;
        return dif;

    }

    @Override
    public String toString() {
        String a = "";
        for (int i = 0; i < dendrites.length; i++) a += (firstWeights[i] - dendrites[i]) == 0 ? "" : dendrites[i] + " ";
        return a;
    }
}
