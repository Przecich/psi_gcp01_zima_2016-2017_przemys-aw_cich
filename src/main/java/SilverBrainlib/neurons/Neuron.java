package SilverBrainlib.neurons;

import SilverBrainlib.ActivationFunctions.ActivationFunction;

public class Neuron extends NeuronModel {

    ActivationFunction function;
    public double threshold = 0;


    public Neuron(int intputCount, ActivationFunction function) {
        super(intputCount);
        this.function = function;
        threshold = rand.nextDouble();
    }


    public ActivationFunction getFunction() {
        return function;
    }

    @Override
    public double compPotential(double[] input) {
        if (input.length != dendritesCount)
            throw new IllegalArgumentException("Wrong size of input data");


        double sum = 0;
        for (int i = 0; i < input.length; i++) {
            sum += dendrites[i] * input[i];

        }
        sum += threshold;
        double output = function.activationfunction(sum);

        this.axon = output;

        return output;
    }
}
