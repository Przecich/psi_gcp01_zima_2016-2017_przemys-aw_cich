package Lambdas;

import java.util.function.Function;

public class Lambda {
    int a = 5;

    public Integer print(Integer b) {
        System.out.println(a + b);
        return null;
    }

    public void blabla() {
        Function<Integer, Integer> converter = this::print;
    }
}
