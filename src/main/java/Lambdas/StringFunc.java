package Lambdas;

public interface StringFunc {
    void run(Integer a, Integer b);
}
