package Lambdas;

import SilverBrainlib.ActivationFunctions.ActivateBipolarSigmoid;
import SilverBrainlib.UnsupervisedLearning.HebbLearning;
import SilverBrainlib.networks.Network;
import SilverBrainlib.networks.NetworkModel;
import SilverBrainlib.neurons.Neuron;

public class HebbanonLearning {

    static double[][] input = new double[][]{
            new double[]{1,1,1,1,0,0},
            new double[]{0,0,0,0,0,0},
            new double[]{1,1,0,0,1,1},

    };

    static double[][] output = new double[][]{
            new double[]{0},
            new double[]{0},
            new double[]{0}

    };


    public static void main(String[] args) {

        NetworkModel network = new Network(new ActivateBipolarSigmoid(),6, 1);
        Neuron neuron = (Neuron) network.getLayers()[0].getNeurons()[0];
        HebbLearning techaer = new HebbLearning(network);
        int i = 0;

        while (i < 10000) {
            double error = techaer.learnEpoch(input, output);
            System.out.println(error);

            i++;
        }
        System.out.println("sdasd");
       System.out.println(neuron.compPotential(new double[]{1,1,1,1,0,0}));
        System.out.println(neuron.compPotential(new double[]{0,0,0,0,0,0}));
        System.out.println("Epochs: " + i);

    }


}
