package Lambdas;

import SilverBrainlib.ActivationFunctions.ActivateBipolarSigmoid;
import SilverBrainlib.learning.BackPropagationLearning;
import SilverBrainlib.networks.Network;

public class Main {
    static double[][] input = new double[][]{
            new double[]{0, 0},
            new double[]{0, 1},
            new double[]{1, 0},
            new double[]{1, 1}
    };
    ;
    static double[][] output = new double[][]{
            new double[]{0},
            new double[]{1},
            new double[]{1},
            new double[]{0}
    };


    public static void main(String[] args) {
        Network network = new Network(new ActivateBipolarSigmoid(),
                2, 2, 1);
        // create teacher
        BackPropagationLearning teacher = new BackPropagationLearning(network);
        // set learning rate and momentum
        teacher.setLearningRate(0.1);
       // teacher.setMomentum(0);

        // iterations
        int iteration = 1;
        for (int i = 0; i < 250; i++) ;
        while (true) {
            // learnRecord epoch of learning procedure
            double error = teacher.learnEpoch(input, output);
            //System.out.println(error);
            //System.out.println(error);
            // save current error
            System.out.println(error);
            // show current iteration & error

            iteration++;

            // check if we need to stop
            if (error <= 0.1)
                break;
        }
    }
}
