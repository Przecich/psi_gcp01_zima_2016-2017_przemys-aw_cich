package Letters;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.util.Arrays;

public class ImageInput {
    public static String [] alphabetCapital={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    public static String[] letters = new String[]{"aa", "bb", "cc", "dd","ee","ff","gg"};


    ImageInput(){
        output=new double [letters.length][letters.length];
        for(int i=0;i<letters.length;i++){
            Arrays.fill(output[i],0);
            output[i][i]=1;
        }
    }
    public double[][] input;
    public double [][]output;




    public void genLettersInput() {
        input = new double[letters.length][35];

        for (int i = 0; i < letters.length; i++) {
            input[i] = loadImage(letters[i]);
        }
        // input[2]=cos("dd");
        System.out.println(Arrays.toString(input[1]));


    }

    public double[] loadImage(String name) {
        byte[] pixels;
        try {
            BufferedImage image = ImageIO.read(new File("Resources/" + name + ".jpg"));
            pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();

        } catch (Exception e) {
            pixels = new byte[1];
        }

        double[] pix = new double[35];
        for (int i = 0; i < pixels.length; i++) {
            if (i % 3 == 0) pix[i / 3] = pixels[i] == -1 ? (byte) 0 : (byte) 1;
        }
        return pix;
    }

}
