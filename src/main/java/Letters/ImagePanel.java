package Letters;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ImagePanel extends JPanel {

    private Image image;

    public ImagePanel() {

        try {
            image = ImageIO.read(new File("Resources/aa.jpg"));

            image = image.getScaledInstance(100, 140, 1);
            //image=Thumbnails.of(image).size(100, 140).asBufferedImage();


        } catch (IOException ex) {
            // handle exception...
            System.out.println("PROBLEMS");
        }
        this.setSize(200, 200);
    }

    public void setImage(String path) {
        try {
            image = ImageIO.read(new File("Resources/" + path + ".jpg"));
            image = image.getScaledInstance(100, 140, 1);
            repaint();
        } catch (Exception e) {
            System.out.println(e.getCause());
        }

    }

    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
    }

}