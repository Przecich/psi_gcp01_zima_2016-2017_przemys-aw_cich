package Letters;

import SilverBrainlib.ActivationFunctions.ActivationFunction;
import SilverBrainlib.learning.BackPropagationLearning;
import SilverBrainlib.learning.Learning;
import SilverBrainlib.networks.Network;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class ErrorWorker extends SwingWorker<Void, ErrorEpoch> {

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getLearningRate() {
        return learningRate;
    }

    private double learningRate = 0.1;

    public void setFun(ActivationFunction fun) {
        this.fun = fun;
    }

    private ActivationFunction fun;

    JTextField textField;
    JTextField textField2;
    Network network;

    ErrorWorker(JTextField textField, JTextField textField2) {
        this.textField = textField;
        this.textField2 = textField2;
    }

    @Override
    protected Void doInBackground() throws Exception {
        ImageInput img = new ImageInput();
        img.genLettersInput();

        network = new Network(fun,
                35, 15, ImageInput.letters.length);
        // create teacher
        Learning teacher = new BackPropagationLearning(network);
        // set learning rate and momentum
        ((BackPropagationLearning)teacher).setLearningRate(learningRate);



        double time = System.currentTimeMillis();
        ArrayList<Double> errors=new ArrayList<>();
        teacher.addErrorListener(e->{errors.add(e);publish(new ErrorEpoch(e,errors.size()));});
        teacher.learn(img.input,img.output);
        System.out.println(System.currentTimeMillis() - time);

        return null;
    }

    @Override
    protected void process(List<ErrorEpoch> chunks) {

        ErrorEpoch pair = chunks.get(chunks.size() - 1);
        textField.setText(String.format("%.6f",pair.first));
        textField2.setText(String.format("%.0f",pair.second));


    }
}
