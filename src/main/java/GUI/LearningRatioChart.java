package GUI;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.ArrayList;

public class LearningRatioChart extends ChartPanel {
    XYSeriesCollection dataset = new XYSeriesCollection();
    public XYSeries series = new XYSeries("Absolute MSE/Cycle");
    public XYSeries series2 = new XYSeries("Validation MSE/Cycle");

    LearningRatioChart() {
        super(null);
        dataset.addSeries(series);
        dataset.addSeries(series2);

        String chartTitle = "Learning Curve";
        String xAxisLabel = "Cycles";
        String yAxisLabel = "Absolute Error";


        JFreeChart chart = ChartFactory.createXYLineChart(chartTitle,
                xAxisLabel, yAxisLabel, dataset);
        this.setChart(chart);


    }

    public void addOrUpdateSeries(double id, double value) {
        series.addOrUpdate(id, value);
    }

    public void addSeries(ArrayList values) {
        for (int i = 0; i < 0; i++) addOrUpdateSeries((double) i, (double) values.get(i));
    }


}
