/*
package GUI;

import SilverBrainlib.ActivationFunctions.ActivateSigmoid;
import SilverBrainlib.learning.PerceptronLearning;
import SilverBrainlib.neurons.Neuron;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class StayHere {
    private JPanel panel1;
    private LearningRatioChart errorChart;
    private JButton button1;
    private JTextArea textArea1;

    double  [][] datainput = {{1,1,1}, {1,1,0}, {1,0,1}, {0,1,1},{1,0,0},{0,1,0},{0,0,1}};
    double [] dataoutput={1,0,0,0,0,0,0};

    double [][] tetrominoData=new double[1000][6];
    double [] tetrominoOutput=new double[1000];
    double [][]testData=new double[300][6];
    double []testOutput=new double[300];

    double[] tetromin={1,1,1,0,0,1};
    double []tetromin2={1,0,0,1,1,1};

    private void genData(double [][]tetrominodata,double [] tetrominooutput){
        Random rand=new Random();
        int n=tetrominooutput.length;
        for(int i=0;i<n;i++)tetrominooutput[i]=0;
        for(int i=0;i<n;i++){
            for(int j=0;j<6;j++){
                tetrominodata[i][j]=rand.nextInt(2);
            }
            if(Arrays.equals(tetrominodata[i],tetromin)  )tetrominooutput[i]=1;
        }
    }
    public StayHere() {
        genData(tetrominoData,tetrominoOutput);
        genData(testData,testOutput);
        System.out.println(Arrays.toString(tetromin));
        Neuron [] neurons=new Neuron[100];
        button1.addActionListener(e -> {
            //for(int i=0;i<100;i++) {
            Neuron neuron = new Neuron(6,new ActivateSigmoid());
            PerceptronLearning teacher = new PerceptronLearning(neuron);
            learning(neuron, teacher);
            textArea1.append("Test " + test(neuron));
            //}
        });
    }
    public void learning(Neuron neuron,PerceptronLearning teacher){
        int i=0;
        try {
            PrintWriter writer = new PrintWriter(new FileOutputStream(
                    new File("ResultsForPerceptron2.txt"),
                    true));
            errorChart.series.clear();
            errorChart.series2.clear();
            textArea1.setText(null);
            PerceptronLearning teacher2 = new PerceptronLearning(neuron);
            int above=0;
            int below=0;
            while (true) {
                double error = teacher.learn(tetrominoData, tetrominoOutput);
                above+=teacher.numAbove;
                below+=teacher.numBelow;
                double error2=teacher2.learn(testData,testOutput);

                if (error <0.1) break;
                //System.out.println(error);
                //for(int l=0;l<neuron.weights.length;l++) System.out.println(("W"+l+" "+neuron.weights[l]));
                //System.out.println(neuron.weights.length);
                i++;
            }
            System.out.println(above);
            System.out.println(below);
            textArea1.append("Neuron was learned\n");
            writer.print("Neuron was learned\n");
            textArea1.append("W1: " + neuron.weights[0] + " W2: " + neuron.weights[1] + "\n");

            for(int l=0;l<neuron.weights.length;l++)writer.print("W"+l+" "+neuron.weights[l]);
            //writer.print("W1: " + neuron.weights[0] + " W2: " + neuron.weights[1] + "\n");

            textArea1.append("Threshold: " + neuron.threshold + "\n");
            writer.print("Threshold: " + neuron.threshold + "\n");
            textArea1.append("ERRORS || MSE || MAPE \n");

            for (int j = 0; j < teacher.errors.size(); j++) {
                errorChart.addOrUpdateSeries(j, (double) teacher.mse.get(j));

                //textArea1.append(teacher.errors.get(j) + " " + teacher.mse.get(j) + " " + teacher.mapes.get(j) + "\n");
                //writer.print(teacher.errors.get(j) + " " + teacher.mse.get(j) + " " + teacher.mapes.get(j) + "\n");
            }


            for (int j = 0; j < teacher2.errors.size(); j++) {
                errorChart.series2.addOrUpdate(j, (double) teacher2.mse.get(j)); */
/*(double) teacher.mse.get(j)*//*

            }
            //textArea1.setText(null);

            System.out.println("Epochs: "+i);
            System.out.println("Neuron was learnt");
            System.out.println(neuron.dendrites[0] + " " + neuron.dendrites[1]);
            System.out.println(((Neuron) neuron).threshold);

            writer.close();
        }
        catch (IOException e){
            System.out.println("EXCEPTION!!!");
        }

    }
    public double test(Neuron neuron){
        PerceptronLearning teacher = new PerceptronLearning(neuron);
        return teacher.learnEpoch(testData,testOutput);
    }

    public static void main(String[] args) {
        Menu menu=new Menu();
        JFrame frame = new JFrame("Menu");
        //frame.setContentPane(menu.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);




    }
}
*/
