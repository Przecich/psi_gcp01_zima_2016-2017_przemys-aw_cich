package GUI;

import SilverBrainlib.ActivationFunctions.ActivateSigmoid;
import SilverBrainlib.Layers.Layer;
import SilverBrainlib.Layers.PrimaryLayer;
import SilverBrainlib.learning.BackPropagationLearning;
import SilverBrainlib.learning.Learning;
import SilverBrainlib.networks.Network;
import SilverBrainlib.networks.NetworkModel;
import SilverBrainlib.neurons.Neuron;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Menu {
    private JPanel panel1;
    private LearningRatioChart errorChart;
    private JButton button1;
    private JTextArea textArea1;

    double[][] datainput = {{1, 1, 1}, {1, 1, 0}, {1, 0, 1}, {0, 1, 1}, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    double[] dataoutput = {1, 0, 0, 0, 0, 0, 0};

    double[][] tetrominoData = new double[1000][6];
    double[][] tetrominoOutput = new double[1000][2];
    double[][] testData = new double[300][6];
    double[][] testOutput = new double[300][2];

    double[] tetromin = {1, 1, 1, 0, 0, 1};
    double[] tetromin2 = {1, 0, 0, 1, 1, 1};

    private void genData(double[][] tetrominodata, double[][] tetrominooutput) {
        Random rand = new Random();
        int n = tetrominooutput.length;
        for (int i = 0; i < n; i++) {
            int l = tetrominooutput[i].length;
            for (int j = 0; j < l; j++)
                tetrominooutput[i][j] = 0;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 6; j++) {
                tetrominodata[i][j] = rand.nextInt(2);
            }
            if (Arrays.equals(tetrominodata[i], tetromin)) tetrominooutput[i][0] = 1;
            if (Arrays.equals(tetrominodata[i], tetromin2)) tetrominooutput[i][1] = 1;
        }
    }

    public Menu() {
        genData(tetrominoData, tetrominoOutput);
        genData(testData, testOutput);
        System.out.println(Arrays.toString(tetromin));
        Neuron[] neurons = new Neuron[100];
        button1.addActionListener(e -> {
            //for(int i=0;i<100;i++) {
            NetworkModel network = new Network(new ActivateSigmoid(), 6, 2);
            Layer layer = new PrimaryLayer((byte) 2, (byte) 6, new ActivateSigmoid());
            Learning teacher = new BackPropagationLearning(network);
            Learning teacher2 = new BackPropagationLearning(network);
            learning(layer, teacher, teacher2);
            System.out.println(teacher.learnEpoch(testData, testOutput));
            //textArea1.append("Test " + test(neuron));
            //}
        });
    }

    public void learning(Layer layer, Learning teacher, Learning teacher2) {
        int i = 0;
        ArrayList<Double> numbers = new ArrayList<>();
        ArrayList<Double> numbers2 = new ArrayList<>();
        try {
            PrintWriter writer = new PrintWriter(new FileOutputStream(
                    new File("ResultsforBack.txt"),
                    true));
            errorChart.series.clear();
            errorChart.series2.clear();
            textArea1.setText(null);
            int above = 0;
            int below = 0;
            while (true) {
                double error = teacher.learnEpoch(tetrominoData, tetrominoOutput);
                double error2 = teacher2.learnEpoch(testData, testOutput);
                if (error < 0.1) break;
                //System.out.println(error);
                //for(int l=0;l<neuron.dendrites.length;l++) System.out.println(("W"+l+" "+neuron.dendrites[l]));
                //System.out.println(neuron.dendrites.length);
                i++;
                System.out.println(error);
                numbers.add(error);
                numbers2.add(error2);
            }
            for (int j = 0; j < numbers.size(); j++) {
                errorChart.addOrUpdateSeries(j, numbers.get(j));

                writer.append(numbers.get(j).toString() + "\n");
            }

            for (int j = 0; j < numbers2.size(); j++)
                errorChart.series2.addOrUpdate(j, (double) numbers2.get(j));
           /* System.out.println(above);
            System.out.println(below);*/
            textArea1.append("Neuron was learned\n");
            //writer.print("Neuron was learned\n");


            System.out.println("Epochs: " + i);
            System.out.println("Neuron was learnt");
            for (int l = 0; l < layer.getNeuronCount(); l++) {
                textArea1.append("Neuron " + (l + 1 + "\n"));
                for (int j = 0; j < layer.getNeurons()[l].dendrites.length; j++) {
                    textArea1.append("Weight " + (j + 1) + layer.getNeurons()[l].dendrites[j] + "\n");
                }
            }
            System.out.println();
            /*System.out.println(neuron.dendrites[0] + " " + neuron.dendrites[1]);
            System.out.println(((Neuron) neuron).threshold);*/

            writer.close();
        } catch (IOException e) {
            System.out.println("EXCEPTION!!!");
        }

    }


    public static void main(String[] args) {
        Menu menu = new Menu();
        JFrame frame = new JFrame("Menu");
        frame.setContentPane(menu.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
        errorChart = new LearningRatioChart();
        panel1.add(errorChart, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        button1 = new JButton();
        button1.setText("GO!");
        panel1.add(button1, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        textArea1 = new JTextArea();
        panel1.add(textArea1, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }
}
